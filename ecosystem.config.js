module.exports = {
    /**
     * Application configuration section
     * http://pm2.keymetrics.io/docs/usage/application-declaration/
     */
    apps : [

        // Production application
        {
            name      : "API",
            script    : "app.js",
            watch     : true,
            ignore_watch: ["log"],
            env: {
                NODE_ENV: "production"
            },
            env_production : {
                NODE_ENV: "production"
            }
        },
        // Development / Debug application
        {
            name      : "API-DEBUG",
            script    : "app.js",
            watch     : true,
            ignore_watch: ["log"],
            env: {
                NODE_ENV: "development"
            },
            env_production : {
                NODE_ENV: "development"
            }
        }
    ]
};
