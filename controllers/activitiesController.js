var helpers = require('../config/helpers.js');
var ActivityModel = require('../models/activityModel');
var UserModel = require('../models/userModel');

const dateField = "date_start";

module.exports = function(server) {
    server.get('/activities', function (req, res, next) {
        var searchFilter = {};
        var sortFilter = {};
        sortFilter[dateField] = 1;

        if (req.query.sport) {
            searchFilter['sport'] = {
                $regex: req.query.sport
            }
        }
        if (req.query.fromDate) {
            // make a date object from the string
            var d = new Date(req.query.fromDate);
            searchFilter[dateField] = {
                // return only results greater than the provided date
                // meaning "future activities"
                $gte: d
            }
        }
        if (req.query.user) {
            // returns only activities with a certain user in the attendees list
            searchFilter['attendees'] = req.query.user;
        }
        ActivityModel.find(searchFilter).sort(sortFilter).exec(function(err, activities) {
            if (err) {
                helpers.failure(res, next, 'Something went wrong', 500);
                return;
            }
            helpers.success(res, next, activities);
        });
    });

    // TODO: implement definition
    // server.get('/activities/:id', function (req, res, next) {
    //     VenueModel.findOne({ _id: req.params.id }, function (err, user) {
    //         if (err) {
    //             helpers.failure(res, next, 'Something went wrong', 500);
    //         }
    //         if (user === null) {
    //             helpers.failure(res, next, 'The specified user could not be found', 404);
    //         }
    //         helpers.success(res, next, user);
    //     });
    // });

    // TODO: make tests
    server.post('/activities', function (req, res, next) {

        var data = req.params;

        // TODO: move this in reusable function
        if (data.sport === 'football') {
            data.sport = 'football / soccer'
        }

        if (!data.place) {
            helpers.failure(res, next, 'Invalid request: you should provide a place', 400);
            return;
        }

        var dataObj = {
            created_at: new Date(),
            sport: data.sport,
            place: data.place,
            date_start: data.date_start,
            date_end: data.date_end,
            host_name: data.host,
            notes: data.notes,
            level: data.level,
            attendees: data.attendees,
            host_id: data.host_id
        };

        ActivityModel.create(dataObj, function(err, activity) {
            if (err) {
                helpers.failure(res, next, 'Error saving activity: ' + err, 500);
            } else {
                helpers.success(res, next, activity);
            }
        });
    });

    server.put('/activities/:id/join', function (req, res, next) {
        // validate the user
        UserModel.findOne({ "device_id": req.params.device_id }, function (err, user) {
            if (err) {
                helpers.failure(res, next, 'Something went wrong', 500);
                return;
            }
            if (user) {
                ActivityModel.findOne({ _id: req.params.id }, function (err, activity) {
                    if (err) {
                        helpers.failure(res, next, 'Something went wrong', 500);
                    } else {
                        if (activity === null) {
                            helpers.failure(res, next, 'The specified activity could not be found', 404);
                        } else {
                            var att = activity.attendees || [];
                            if (att.indexOf(user._id.toString()) < 0) {
                                att.push(user._id.toString());
                                activity.save(function (err) {
                                    if (err) {
                                        helpers.failure(res, next, err);
                                    } else {
                                        helpers.success(res, next, activity);
                                    }
                                });
                            } else {
                                helpers.success(res, next, activity);
                            }
                        }
                    }
                });
            } else {
                helpers.failure(res, next, 'Unauthorized', 404);
            }
        });
    });


    // TODO: reuse code
    // TODO: comments
    // TODO: unit tests
    server.put('/activities/:id/leave', function (req, res, next) {
        // validate the user
        UserModel.findOne({ "device_id": req.params.device_id }, function (err, user) {
            if (err) {
                helpers.failure(res, next, 'Something went wrong', 500);
                return;
            }
            if (user) {
                ActivityModel.findOne({ _id: req.params.id }, function (err, activity) {
                    if (err) {
                        helpers.failure(res, next, 'Something went wrong', 500);
                    } else {
                        if (activity === null) {
                            helpers.failure(res, next, 'The specified activity could not be found', 404);
                        } else {
                            var att = activity.attendees || [];
                            var index = att.indexOf(user._id.toString());
                            if (index >= 0) {
                                att.splice(index, 1);
                                activity.save(function (err) {
                                    if (err) {
                                        helpers.failure(res, next, err);
                                    } else {
                                        helpers.success(res, next, activity);
                                    }
                                });
                            } else {
                                helpers.success(res, next, activity);
                            }
                        }
                    }
                });
            } else {
                helpers.failure(res, next, 'Unauthorized', 404);
            }
        });
    });
};