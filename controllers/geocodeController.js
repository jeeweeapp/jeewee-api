const fetch = require('node-fetch')
const helpers = require('../config/helpers.js')
const API_KEY = 'AIzaSyBtmwQcO1cF-up4InmJI29GPzaFTFj3E6c'
const GMAPS_API_BASEURL = `https://maps.googleapis.com/maps/api/geocode/json?key=${API_KEY}`

const parseGeocodeData = item => {
    let addressComponents = item.address_components

    const geocodedResult = {
        coord: {
            n: item.geometry.location.lat,
            e: item.geometry.location.lng
        }
    }

    addressComponents.map(component => {
        const types = component.types
        if (types.includes('street_number')) {
            geocodedResult.number = component.long_name
        } else if (types.includes('route')) {
            geocodedResult.street = component.long_name
        } else if (types.includes('postal_code')) {
            geocodedResult.zip = component.long_name
        } else if (types.includes('country')) {
            geocodedResult.country = component.long_name
        } else if (types.includes('locality')) {
            geocodedResult.city = component.long_name
        }
    })

    return geocodedResult
}

module.exports = function(server) {
    server.get('/geocode', function (req, res, next) {
        // TODO: search first in a cache (db?) to avoid hitting the API many times
        // Italy currently set as default
        const locality = req.params.locality
        let search = req.params.search
        const country = req.params.country

        if (locality) {
            search += locality
        }

        let url = `${GMAPS_API_BASEURL}&address=${search}`

        if (country) {
            url += `&components=country:${country}`
        }

        return fetch(
            url,
            {
                method: 'GET'
            }
        )
            .then(response => {
                if (response.ok) {
                    return response.json()
                }
                throw 'Bad response from Geocode API'
            })
            .then(data => {
                const { results } = data
                const parsedResults = results.map(item => parseGeocodeData(item))
                helpers.success(res, next, parsedResults)
            })
            .catch(error => {
                helpers.failure(res, next, error)
            })
    })

    server.get('/reverseGeocode', (req, res, next) => {
        const lat = req.params.lat
        const lng = req.params.lng

        const url = `${GMAPS_API_BASEURL}&latlng=${lat},${lng}&result_type=administrative_area_level_3|locality|country`

        return fetch(
            url,
            {
                method: 'GET'
            }
        )
            .then(response => {
                if (!response.ok) {
                    throw {
                        code: response.status,
                        message: response.statusText
                    }
                }
                return response.json()

            })
            .then(data => {
                const { results } = data
                if (!results.length) {
                    throw 'No results'
                }
                const components = results[0].address_components
                const result = {}
                components.map(component => {
                    const types = component.types
                    if (types.includes('country')) {
                        result.country = component.short_name
                    } else if (types.includes('administrative_area_level_3')) {
                        result.locality = component.long_name
                    } else if (types.includes('locality')) {
                        result.locality = component.long_name
                    }
                })
                if (!result.country) {
                    return helpers.failure(res, next, 'Country is missing')
                }
                return helpers.success(res, next, result)
            })
            .catch(error => {
                return helpers.failure(res, next, error)
            })
    })
}