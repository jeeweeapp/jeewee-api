var auth = require('../utils/auth');
var helpers = require('../config/helpers');

var UserModel = require('../models/userModel');

module.exports = function(server) {
    /**
        Receives a device id and creates an user with it.
        For the first phase, there is no username and password based registration.
        If the device gets stolen, we can disable the device_id (if we know it)
     */
    server.post('/users', function (req, res, next) {
        var deviceId = req.params.device_id;
        if (!deviceId) {
            helpers.failure(res, next, 'You must provide a device ID', 400);
        }
        UserModel.findOne({ "device_id": deviceId }, function (err, user) {
            if (err) {
                helpers.failure(res, next, 'Something went wrong', 500);
            }
            if (user) {
                // there is already this user
                helpers.success(res, next, user);
            } else {

                // to simplify the process and prevent loops,
                // when we make a new user we also return the initial token
                var jwt = auth.createTokenFor(deviceId);

                // create the new user
                UserModel.create({
                    device_id: deviceId,
                    created_at: new Date().toISOString(),
                    enabled: true,
                    initialToken: jwt
                }, function(err, user) {
                    if (err) {
                        helpers.failure(res, next, 'Error saving user', 500);
                    } else {
                        helpers.success(res, next, user);
                    }
                });
            }
        });
    });
};