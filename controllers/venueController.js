var helpers = require('../config/helpers.js');
var VenueModel = require('../models/venueModel');
var gps = require('../utils/gps');

const venueTypesMapping = {
    sport_center: 'Sport Center',
    playground: 'Playground',
    aquatic_center: 'Aquatic Center',
    park: 'Park',
    fitness_park: 'Fitness Park',
    skatepark: 'Skate Park',
    sport_club: 'Sport Club'
};

function sortByDistance(venues, origin) {
    venues.forEach(function(venue, index){
        let venueCoord = venue.coord;
        if (venueCoord !== null) {
            venue['distance'] = gps.getLinearDistanceBetween(origin, venueCoord);
        }
        venues[index] = venue;
    });

    sortByKey(venues, 'distance');

    return venues;
}

// TODO: move to utils
function sortByKey(array, key) {
    return array.sort(function(a, b) {
        var x = a[key]; var y = b[key];
        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });
}

function mapVenueTypes(venues) {
    venues.forEach(function(venue, index){
        if (venue.type) {
            try {
                venue['type'] = venueTypesMapping[venue.type];
                venues[index] = venue;
            } catch (e) {
                console.warn('Venue type ' + venue.type + ' is not valid');
                console.warn(e);
            }
        }
    });

    return venues;
}

module.exports = function(server) {

    // TODO: unit tests
    server.get('/venues', function (req, res, next) {

        var data = req.params;
        var searchFilter = {};

        if (data.sport) {
            searchFilter['sports'] = {
                $regex: data.sport
            }
        }

        VenueModel.find(searchFilter, function (err, venues) {

            if (data.e && data.n) {
                // sort venues by distance from the geo point e,n
                sortByDistance(venues, {e: data.e, n: data.n});
            }

            if (data.limit) {
                venues = venues.splice(0, data.limit);
            }

            mapVenueTypes(venues);

            if (err) {
                helpers.failure(res, next, 'Something went wrong', 500);
            }
            helpers.success(res, next, venues);
        });
    });
};