var helpers = require('../config/helpers.js')
var env = process.env.NODE_ENV || 'development'
var config = require('../config/config.' + env + '.js')
var auth = require('../utils/auth')

var response = {}

module.exports = function(server, restify) {
    server.use(restify.acceptParser(server.acceptable))
    server.use(restify.bodyParser())
    server.use(restify.queryParser())
    server.use(restify.authorizationParser())

    server.use(restify.requestLogger(
        {
            properties: {
                streams: [
                    {
                        path: 'log/info_' + env + '.log',
                        level: 'info'
                    }
                ],
            },
            serializers: restify.bunyan.serializers
        }
    ))

    server.use(function(req, res, next){
        if ((req.url !== '/users' &&
            req.url !== '/tokens') && !(env === 'development' && req.params.skipToken)) {
            // check if we have a valid token
            var token = req.authorization.credentials
            if (!token) {
                response = {
                    'status': 'failure',
                    'data': 'You must provide a valid token'
                }
                res.setHeader('content-type', 'application/json')
                res.writeHead(401)
                res.end(JSON.stringify(response))
            } else if (!auth.verifyToken(token)) {
                response = {
                    'status': 'failure',
                    'data': 'Invalid token'
                }
                res.setHeader('content-type', 'application/json')
                res.writeHead(401)
                res.end(JSON.stringify(response))
            } else {
                return next()
            }
        } else {
            return next()
        }
    })

    /**
     * Restify uses the token bucket algorithm to throttle traffic.
     * With this, the burst value is the maximum possible number of requests per second,
     * and the rate value is the average rate of requests per second.
     * Even if requests are steady from a caller's perspective,
     * the Restify server may not receive those requests at a steady pace
     * (due to transmission congestion or other reasons),
     * so the burst value provides some tolerance level beyond the average rate value.
     *
     * TODO: set some reasonable values
     */
    server.use(restify.throttle({
        rate: 0,
        burst: 0,
        ip: true
    }))

    server.get('/', function (req, res, next) {
        helpers.success(res, next, 'Welcome!')
    })
}