var auth = require('../utils/auth');
var helpers = require('../config/helpers');

var UserModel = require('../models/userModel');

module.exports = function(server) {
    server.post('/tokens', function (req, res, next) {
        var deviceId = req.params.device_id;
        if (!deviceId) {
            helpers.failure(res, next, 'You must provide a device ID', 400);
        }
        UserModel.findOne({ "device_id": deviceId }, function (err, user) {
            if (err) {
                helpers.failure(res, next, 'Something went wrong', 500);
            }
            if (!user) {
                // there is already this user
                helpers.failure(res, next, 'User not registered', 400);
            } else {
                var jwt = auth.createTokenFor(deviceId);
                var token = {
                    "jwt_token": jwt,
                    "user_id": user._id.toString() // sending here also the ID that we need to identify the joined activities
                };
                helpers.success(res, next, token);
            }
        });
    });
};