var restify = require('restify')
var server
var env = process.env.NODE_ENV || 'development'

var server_options = {}

var fs = require('fs')
var config = require('./config/config.' + env + '.js')

if (config.https) {
    server_options = {
        key: fs.readFileSync(config.https.key),
        certificate: fs.readFileSync(config.https.certificate)
    }
}

server = restify.createServer(server_options)

var setupController = require('./controllers/setupController.js')
var venueController = require('./controllers/venueController.js')
var activitiesController = require('./controllers/activitiesController.js')
var usersController = require('./controllers/usersController.js')
var tokensController = require('./controllers/tokensController.js')
var geocodeController = require('./controllers/geocodeController.js')

var mongoose = require('mongoose')
var dbconfig = require('./config/dbConnections')
var mongooseOptions = {
    server: {
        socketOptions: {
            keepAlive: 300000,
            connectTimeoutMS: 30000
        }
    }
}
mongoose.connect(dbconfig.getMongoConnection(), mongooseOptions)
var conn = mongoose.connection
conn.on('error', console.error.bind(console, 'Database connection error.'))


//conn.once('open', function() {
// Cannot wait for now untill the db opens
// becase the unit tests are not waiting for async
// behavior. TODO: fix this

setupController(server, restify)
venueController(server)
activitiesController(server)
usersController(server)
tokensController(server)
geocodeController(server)

server.listen(config.port, function() {
    console.log('%s listening at %s', server.name, server.url)
})