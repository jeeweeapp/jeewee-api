/**
 * We might want to mock the mongo connection with https://github.com/williamkapke/mongo-mock
 * For now its OK so we test also the connection (even though this is not a unit test)
 */

var env = process.env.NODE_ENV || 'development';
var config = require('../config/config.' + env + '.js');
var params = require('../config/params.js');

var jsdiff = require('diff');

var request = require("request");
var mockedActivities = require('./mock/activities.json');
var matchers = require('../utils/matchers');

// special token for testing purposes set to expire in 1000 days
var tokenModule = require('./token.js');
var token = tokenModule.token;

/*
    Start the app with test env
 */
require('../app.js');

describe("Activities API", function() {

    beforeEach(function(){
        jasmine.addMatchers({
            toBeLike: matchers.toBeLike
        });
    });

    it("should get all the activities", function(done) {
        request.get(
            {
                uri: config.host + ':' + config.port + '/activities',
                json: true,
                auth: {
                    bearer: token
                }

            },
            function(error, response) {
                if (error) {
                    done.fail(error);
                }
                expect(response).toBeLike(mockedActivities);
                done();
            }
        );
    });

    it("should get only tennis activities", function(done){
        request.get(
            {
                uri: config.host + ':' + config.port + '/activities',
                json: true,
                auth: {
                    bearer: token
                },
                qs: {
                    sport: 'tennis'
                }

            },
            function(error, response) {
                if (error) {
                    done.fail(error);
                }
                expect(response).toBeLike([mockedActivities[1]]);
                done();
            }
        );
    })
});
