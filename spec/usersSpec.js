/**
 * We might want to mock the mongo connection with https://github.com/williamkapke/mongo-mock
 * For now its OK so we test also the connection (even though this is not a unit test)
 */

var env = process.env.NODE_ENV || 'development';
var config = require('../config/config.' + env + '.js');
var params = require('../config/params.js');

var request = require("request");
var mockedUser = require('./mock/usersPost.json');
var mockedUserResposne = require('./mock/userGet.json');

var matchers = require('../utils/matchers');

/*
 Start the app with test env
 */
require('../app.js');

describe("Users API", function() {

    beforeEach(function(){
        jasmine.addMatchers({
            toBeLike: matchers.toBeLike
        });
    });

    it("should create an user given a device id", function(done) {
        request.post(
            {
                uri: config.host + ':' + config.port + '/users',
                json: true,
                body: mockedUser
            },
            function(error, response) {
                if (error) {
                    done.fail(error);
                }

                expect(response.body.data.device_id).toBe(mockedUser.device_id);
                expect(response.body.data.enabled).toBe(true);
                done();
            }
        );
    });
});
