var mongoose = require('mongoose');

var Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var UserSchema = new Schema({
    id: ObjectId,
    device_id: String,
    created_at: Date,
    enabled: Boolean,
    initialToken: String
});

var UserModel = mongoose.model('User', UserSchema);

module.exports = UserModel;