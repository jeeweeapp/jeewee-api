var mongoose = require('mongoose');
var sports = require('../data/sports');

// using native promises
// see: http://mongoosejs.com/docs/promises.html
mongoose.Promise = global.Promise;

var Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var ActivitySchema = new Schema({
    id: ObjectId,
    host_id: {
        type: ObjectId,
        required: true
    },
    created_at: {
        type: Date,
        required: true
    },
    sport: {
        type: String,
        enum: sports,
        required: true
    },
    /**
     * place can be a simple address or a more complex Venue object.
     * "place": {
            "address": "Am Bongert, 28",
            "coord": {
                "e": 6.162589,
                "n": 49.604307
            }
        },
        or
     "place": {
            "address": "Dommeldange, 28",
            "coord": {
                "e": 6.162589,
                "n": 49.604307
            },
            "name": "Skatepark Dommeldange",
            ...
            ...
        },
     * TODO: how to validate ?
     */
    place: Schema.Types.Mixed,
    date_start: {
        type: Date,
        required: true
    },
    date_end: {
        type: Date,
        required: true
    },
    level: {
        type: String,
        enum: ['any', 'beginner', 'average', 'expert'],
        default: 'any'
    },
    attendees: [String],
    host_name: String,
    notes: String
});

var ActivityModel = mongoose.model('Activity', ActivitySchema);

module.exports = ActivityModel;