var mongoose = require('mongoose');

var Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var VenueSchema = new Schema({
    id: ObjectId,

    // not sure that is part of the model since it changes at each call, but I need it to console.log
    distance: Number,
    type: {
        type: String,
        enum: ['playground', 'sport_center', 'sport_club', 'skatepark', 'park', 'aquatic_center', 'fitness_park']
    },
    name: String,
    address: {
        "city": String,
        "street": String,
        "number": String,
        "zip": String
    },
    sports: Array,
    coord: {
        e : Number,
        n : Number
    },
    opening: {
        "now": Boolean,
        "default": [
            {
                "start": String,
                "end": String
            }
        ],
        "except": {
            required: false,
            type: [
                {
                    "day": {
                        type: Number,
                        enum: [1, 2, 3, 4, 5, 6, 7]
                    },
                    "hours": {
                        type: [
                            {
                                "start": String,
                                "end": String
                            }
                        ]
                    }
                }
            ]
        }
    },
    phone: String,
    website: String,
    indoor: Boolean,
    outdoor: Boolean,
    fee: Boolean,
    booking: Boolean
});

var VenueModel = mongoose.model('Venue', VenueSchema);

module.exports = VenueModel;