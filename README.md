# Just Join Backend

Simple nodejs API

## Deployment

Every time we push to MASTER, Codeship is configured to run the test and automatically deploy on the DO server.

In the server, pm2 watches for file changes and reloads the node application automatically.

Do NOT push on master changes that are not meant to go on production.

## Debug version
Another pm2 process runs with the 'development' configuration.

This uses the port 3001 and connects to the develoment server.

It is then bound to api-debug.jeeweeapp.com and that's the endpoint connected to the mobile app in "debug" build.

In this way the dev server can be used freely from any place for testing and debugging purposes.

## PM2 info

PM2 can be started / restarted using the ecosystem.config.js file shipped with the app.

    pm2 kill
    pm2 start ecosystem.config.js
    
This will kill all the existing pm2 processes and
restart 1 DEBUG API and 1 PROD API with the proper NODE_ENV variables set.

## Troubleshooting

Check pm2 logs

    pm2 log --err --lines 1000

## Dev notes
### Run the API

    nodemon app.js

### Mongo

Mongoose automatically looks for the plural version of your model name. Thus, for the example above, the model Venue is for the venues collection in the database.


## Test

    npm test
    
## Extra

### JSON CLI manipulation

    cat addresses.geojson | json -c 'this.properties.localite == "Luxembourg"' > luxembourg.json
    cat luxembourg.json | jq 'map({coord: { e: .geometry.coordinates[0][0], n: .geometry.coordinates[0][1]}, rue: .properties.rue, number: .properties.numero, zip: .properties.code_postal, city: .properties.localite})' -c > final.min.json
    
### Database update (PROD)

Import collection
    
    mongoimport -h ds161518.mlab.com:61518 -d augustiner-prod -c venues -u <user> -p <password> --jsonArray --file venues.json

Export collection

    mongoexport -h ds161518.mlab.com:61518 -d augustiner-prod -c venues -u <user> -p <password> -o venues-export.json