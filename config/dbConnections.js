var env = process.env.NODE_ENV || 'development';
var config = require('../config/config.' + env + '.js');

module.exports = {
    getMongoConnection: function(){
        var url = 'mongodb://' + config.database.user + ':' + config.database.password + '@' + config.database.host + ':' + config.database.port + '/' + config.database.db;
        return url;
    }
};