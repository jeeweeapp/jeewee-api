function _respond(res, next, status, data, http_code) {
    var response = {
        status: status,
        data: data
    }
    res.setHeader('content-type', 'application/json')
    res.writeHead(http_code)
    res.end(JSON.stringify(response))
    return next()
}

module.exports.success = function success(res, next, data) {
    var req = res.req
    req.log.info(
        {
            method: req.method,
            url: req.url,
            params: req.params,
            token: req.authorization.credentials,
            results: data.length,
            statusCode: 200
        },
        'success'
    )
    _respond(res, next, 'success', data, 200)
}

module.exports.failure = function failure(res, next, data, http_code = 500) {
    res.req.log.error(data)
    _respond(res, next, 'failure', data, http_code)
}