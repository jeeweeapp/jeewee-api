/**
 * See http://stackoverflow.com/a/21623206/2439580
 * @param pointA
 * @param pointB
 * @returns {number}
 */
function getLinearDistanceBetween(pointA, pointB) {
    let lat1 = pointA.n;
    let lat2 = pointB.n;
    let lon1 = pointA.e;
    let lon2 = pointB.e;

    let p = 0.017453292519943295;    // Math.PI / 180
    let c = Math.cos;
    let a = 0.5 - c((lat2 - lat1) * p)/2 +
        c(lat1 * p) * c(lat2 * p) *
        (1 - c((lon2 - lon1) * p))/2;

    let result = 12742 * Math.asin(Math.sqrt(a)); // 2 * R; R = 6371 km
    return Math.round(result * 100) / 100;
}

module.exports = {
    getLinearDistanceBetween: getLinearDistanceBetween
};