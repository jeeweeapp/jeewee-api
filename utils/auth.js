var jwt = require('jsonwebtoken');

var secret = 'defaultHMACSHA256';

module.exports = {
    createTokenFor: function(deviceid) {
        /*
            (Asynchronous) If a callback is supplied, callback is called with the err or the JWT.
            (Synchronous) Returns the JsonWebToken as string
         */
        return jwt.sign(
            {
                "device_id": deviceid
            },
            secret,
            {
                issuer: 'https://www.jeeweeapp.com',
                expiresIn: '20d' // since we are in beta test, I leave a long expiration to simplify the process
            }
        );
    },

    verifyToken: function(token) {
        try {
            return jwt.verify(token, secret);
        } catch(err) {
            return false;
        }
    }
};