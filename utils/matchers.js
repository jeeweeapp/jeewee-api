var jsdiff = require('diff');

module.exports = {
    toBeLike: function() {
        return {
            compare: function(actual, expected) {
                var data = actual.body.data;
                var dataString = JSON.stringify(data);
                var expectedString = JSON.stringify(expected);
                var result = {
                    pass: actual.statusCode === 200 && dataString === expectedString
                };
                if(result.pass) {
                    result.message = "Status code 200 and body matches";
                } else {
                    result.message =
                        "Status code: " + actual.statusCode;
                    process.stderr.write(dataString);
                    console.log('\n\n\n');
                    process.stderr.write(expectedString);
                }
                return result;
            }
        }
    }
};